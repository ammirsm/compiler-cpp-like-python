import re
from .token import *

class LexicalAnalyzer:

    def __init__(self, file_path):
        self.file = open(file_path,"r")
        self.fileTokens = self.file.read()
        ## Read tokens to list from file
        self.fh = open("./lexical_analyzer/special_token.txt","r")
        self.specialTokenList = self.fh.read().split()
        self.keywordList = ["if", "else", "while", "for"]
        self.pointer = -1
        self.size = len(self.fileTokens)
        self.fileTokens += " $"

    def print_token(self):
        print(self.fileTokens[0])

    ### is number function
    def is_number(self,s):
        try:
            float(s)
            return True
        except ValueError:
            pass

        try:
            import unicodedata
            unicodedata.numeric(s)
            return True
        except (TypeError, ValueError):
            pass
        return False

    def nextChar(self):
        if(self.pointer < self.size):
            self.pointer +=1
            return 1
        else:
            return 0

    # Get next token
    def nextToken(self):
        value = ""
        while(self.nextChar()):
            value += self.fileTokens[self.pointer]
            if value in self.specialTokenList:
                return Token(TokenType.SPECIAL_TOKEN, value)
            elif value is '"':
                self.nextChar()
                value += self.fileTokens[self.pointer]
                while(value[-1] is not '"'):
                    self.nextChar()
                    value += self.fileTokens[self.pointer]
                if len(value) > 1:
                    return Token(TokenType.STRING, value)
            elif self.is_number(value):
                while(self.is_number(self.fileTokens[self.pointer+1])):
                    value += self.fileTokens[self.pointer+1]
                    self.nextChar()
                return Token(TokenType.DIGIT, value.strip())
            elif value.strip() is "$":
                return Token(TokenType.FINAL, value.strip())
            elif value[-1] in self.specialTokenList:
                self.pointer -= 1
                value = value [:-1]
                return Token(TokenType.ID, value.strip())
            elif value.strip() == "":
                value = ""
            elif value[-1] == " ":
                if value.strip() == "":
                    value = ""
                else:
                    return Token(TokenType.ID, value.strip())
            elif value.strip() in self.keywordList:
                return Token(TokenType.KEYWORD, value.strip())
