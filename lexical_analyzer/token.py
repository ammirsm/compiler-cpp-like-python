from enum import Enum

### Tokens
class TokenType(Enum):
     ID = 1
     STRING = 2
     DIGIT = 3
     SPECIAL_TOKEN = 4
     FINAL = 5
     KEYWORD = 6

class Token:
    def __init__(self, type, value):
        self.type = type
        self.value = value

    def get_value(self):
        return self.value + " " + str(self.type)

    def get_type(self):
        return self.type
