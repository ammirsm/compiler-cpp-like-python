import subprocess
import collections as cs
import sys
from lexical_analyzer.lexical import *

gram = cs.defaultdict(list)
fi = cs.defaultdict(list)
fo = cs.defaultdict(list)
pre = cs.defaultdict(list)
table = {}
dic = {}
vars = []
gn = 0

class Stack:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        return self.items[len(self.items)-1]

    def size(self):
        return len(self.items)

def first(var):
	if fi[var]:
		return fi[var]
	arr = []
	for production in gram[var]:
		if production == "lambda":
			arr.append("lambda")
			continue
		for st in production.split():
			if st not in vars:
				arr.append(st)
				break
			else:
				arr.extend(first(st))
				if "lambda" not in fi[st]:
					break
	arr = list(set(arr))
	fi[var] = arr
	return arr

def follow(var):
	if fo[var]:
		return fo[var]
	arr = []
	if var == "P":
		arr.append("$")
	for variable in vars:
		for production in gram[variable]:
			need = 0
			for st in production.split():
				if need == 1:
					if st not in vars:
						arr.append(st)
						need = 0
						break
					else:
						arr.extend(fi[st])
						if "lambda" not in fi[st]:
							need = 0
							break
				elif st == var:
					need = 1
			if need==1:
				if variable!=var:
					arr.extend(follow(variable))
					
	arr = list(set(arr))
	fo[var] = arr
	return arr

def predict(num):
	if pre[num]:
		return pre[num]
	arr = []
	temp = dic[num].split(" -> ")
	flag = 1
	for st in temp[1].split():
		if st == "lambda":
			break
		elif st not in vars:
			arr.append(st)
			flag = 0
			break
		elif "lambda" not in fi[st]:
			arr.extend(fi[st])
			flag = 0
			break
		else:
			arr.extend(fi[st])
	if flag == 1:
		arr.extend(fo[temp[0]])

	arr = list(set(list(arr)))
	#print(arr)
	pre[num] = arr
	return arr

def generate_table():
	for var in vars:
		table.update({var:{}})
	for i in dic:
		for j in pre[i]:
			table[dic[i].split(" -> ")[0]][j] = i



subprocess.call(['notepad.exe', 'inp.txt'])
it = 1
with open("C:\\Users\\malireza97\\Desktop\\inp.txt") as file:
	lines = file.readlines()
	for line in lines:
		temp = line.split(" -> ")
		gram[temp[0].strip()].append(temp[1].strip())
		dic[it] = line
		it+=1
		vars.append(temp[0].strip())


'''last = '0'
for i in dic:
	if last == dic[i].split(" -> ")[0]:
		print("     ",dic[i].split(" -> ")[1])
	else:
		print(dic[i].split(" -> ")[0],dic[i].split(" -> ")[1])
		last = dic[i].split(" -> ")[0]'''

vars = list(set(vars))

for i in vars:
	first(i)
for i in vars:
	follow(i)

for i in dic:
	predict(i)

for i in vars:
	if "lambda" in fi[i]:
		fi[i].remove("lambda")
	if "lambda" in fo[i]:
		fo[i].remove("lambda")

for i in vars:
	print(i, fi[i],fo[i])

for i in dic:
	if "lambda" in pre[i]:
		pre[i].remove("lambda")
	print(i,pre[i])

generate_table()
print(table)

ss = Stack()
ss.push("$")
ss.push("P")
lex = LexicalAnalyzer("test.sbucs")


the_token = lex.nextToken().get_value()
while(the_token is not "$"):
	#print(the_token)
	temp = the_token.split()
	print(ss.peek(),temp[0])
	if ss.peek() in vars:
		if table[ss.peek()][temp[0]]:
			rule = table[ss.peek()][temp[0]]
			print('rule', rule)
			products = dic[rule].split(" -> ")[1]

			for r in reversed(products.split()):
				ss.push(r)
			continue
		else:
			print(ss.peek(), temp[0], "Error!")
			break
	else:
		if ss.peek() != temp[0]:
			print(ss.peek(), temp[0], "Error!")
			print(1)
			break
		elif ss.peek()=="$":
			print("Accepted")
			break
		else:
			print("Popped!",ss.peek())
			ss.pop()
	try:
		the_token = lex.nextToken().get_value()
	except:
		if ss.peek()=="$":
			print("Accepted")
		break


